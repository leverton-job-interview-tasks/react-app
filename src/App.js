import React, { Component } from 'react';
import logo from './logo-color.png';
import './App.css';

var types = [{
    id: 1,
    name: "LONG"
}, {
    id: 2,
    name: "TEXT"
}, {
    id: 3,
    name: "DATE" 
}, {
    id: 4,
    name: "DOUBLE"
}]


var attributes = [{
    id: 1,
    value: 20,
    type: 1,
    name: "Years of contract"
}, {
    id: 5,
    value: "Renter only",
    type: 2,
    name: "Condition"
}, {
    id: 10,
    value: new Date(),
    type: 3,
    name: "Leasing start"
}, {
    id: 12,
    value: 2,
    type: 2,
    name: "Number of properties",
    
}, {
    id: 20,
    value: 2.50,
    type: 4,
    name: "Average height"
}];

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Attribute viewer</h2>
        </div>
        <p className="App-intro">
          The goal of this test is to edit this project to display a list of attributes from different types and provide CRUD ("Create, Read, Update, Delete") functionality. The JS attributes variable at the top of App.js is to be considered as backend. 
        </p>
        <h3>Requirements</h3>
        <ul>
            <li>React is used to create components</li>
            <li>It's possible to edit an attribute's value</li>
            <li>It's possible to delete an attribute</li>
            <li>It's possible to add a new attribute with a type from the list</li>
            <li>Attributes have simple validation based on their types (no need to be too strict, but show that different validations take place for each type)</li>
        </ul>
        <h3>Nice to have</h3>
        <ul>
            <li>Clean separation of code in different classes/files</li>
            <li>Tests in App.test.js</li>
            <li>Use local storage to keep the list of attributes upon page refresh</li>
            <li>A nice interface is appreciated but design is not part of this test so no need to go overboard.</li>
        </ul>
        <h3>Not required</h3>
        <ul>
            <li>No need to use a state manager like Redux here, keep things simple</li>
        </ul>
        <h3>Basic mockup</h3>
          <p>The final ui should present the list of attributes and implement CRUD functionality</p>
          <div>
              <div>Attribute 1 <input name="attribute1" value="value 1"/></div>
              <div>Attribute 2 <input name="attribute2" value="value 2"/></div>
              <div>Attribute 3 <input name="attribute3" value="value 3"/></div>
          </div>
          <h1>Good luck!</h1>
      </div>
    );
  }
}

export default App;
